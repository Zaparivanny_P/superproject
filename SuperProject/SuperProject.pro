#-------------------------------------------------
#
# Project created by QtCreator 2016-10-03T21:47:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SuperProject
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    materialmodel.cpp \
    databasereader.cpp

HEADERS  += mainwindow.h \
    materialmodel.h \
    databasereader.h
