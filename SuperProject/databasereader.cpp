#include "databasereader.h"
#include <QFile>
#include <QTextStream>
#include <QXmlStreamReader>
#include <QDebug>

DataBaseReader::DataBaseReader(QObject *parent) : QObject(parent)
{

}

void DataBaseReader::load(const QString &fileName, MaterialModel *model)
{
    m_model = model;
    QString dataBase;
    dataBase = readXmlPart(fileName);
    //qDebug() << "[DataBaseReader::load]" << dataBase;

    int c = m_model->columnCount();
    m_model->insertColumn(c);
    m_model->setHeaderData(c, Qt::Horizontal, "Name");

    QXmlStreamReader xml;
    xml.addData(dataBase);
    while(xml.atEnd() == false)
    {
        QXmlStreamReader::TokenType token = xml.readNext();
        if (token == QXmlStreamReader::StartElement)
        {
            //qDebug() << "[DataBaseReader::load]" << xml.name().toString();

            QString name = xml.name().toString();
            if(name == "Template")
            {
                QXmlStreamAttributes x = xml.attributes();
                for(auto it : x)
                {
                    //qDebug() << it.name().toString() << it.value().toString();
                    if(it.name().toString() == "Name" && it.value().toString() == "Wurtzite")
                    {
                        readProperty(xml);
                    }
                }
            }
            else if(name == "Material")
            {
                QXmlStreamAttributes x = xml.attributes();
                for(auto it : x)
                {


                    if(it.name().toString() == "Name")
                    {
                        int row = m_model->rowCount();
                        m_model->insertRow(row);

                        QVariant variant = it.name().toString();
                        QByteArray mName = variant.toByteArray();

                        m_model->setData(m_model->index(row, 0), it.value().toString(), mName);
                    }
                }

                readMaterials(xml);
            }
        }
        else if(token == QXmlStreamReader::EndElement)
        {

        }

    }

}

void DataBaseReader::readProperty(QXmlStreamReader &xml)
{

    while(1)
    {
        QXmlStreamReader::TokenType token = xml.readNext();
        if(xml.name().toString() != "Property")
        {
            break;
        }
        if (token == QXmlStreamReader::StartElement)
        {
           // qDebug() << "[DataBaseReader::readProperty]" <<
            QXmlStreamAttributes x = xml.attributes();
            for(auto it : x)
            {
                //qDebug() << "[DataBaseReader::readProperty]" << it.name().toString() << it.value().toString();
                if(it.name() == "Name")
                {
                    int c = m_model->columnCount();
                    m_model->insertColumn(c);
                    m_model->setHeaderData(c, Qt::Horizontal, it.value().toString());
                }
            }
        }
    }
}

void DataBaseReader::readMaterials(QXmlStreamReader &xml)
{
    int row = m_model->rowCount() - 1;
    while(1)
    {

        QXmlStreamReader::TokenType token = xml.readNext();
        if(xml.name().toString() != "Property")
        {
            break;
        }
        if (token == QXmlStreamReader::StartElement)
        {
           // qDebug() << "[DataBaseReader::readProperty]" <<
            QByteArray mName;
            QString mValue;
            QXmlStreamAttributes x = xml.attributes();
            for(auto it : x)
            {
                //qDebug() << "[DataBaseReader::readProperty]" << it.name().toString() << it.value().toString();
                if(it.name() == "Name")
                {
                    QVariant variant = it.value().toString();
                    mName = variant.toByteArray();
                }

                if(it.name() == "value")
                {
                    mValue = it.value().toString();
                }
            }

            m_model->setData(m_model->index(row, 0), mValue, mName);
        }
    }
}

QString DataBaseReader::readXmlPart(const QString &fileName)
{
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "[DataBaseReader::readXmlPart] Error" << file.errorString();
        return QString();
    }

    QTextStream in(&file);
    QString str, dataBase;

    bool isDataBase = false;
    while (!in.atEnd())
    {
        str = in.readLine();
        trimSpaces(str);
        if ((isDataBase == false) && (str == "<MaterialProperties>"))
        {
            isDataBase = true;
        }
        if (isDataBase == true)
        {
            dataBase = dataBase + str;
        }
        if (str == "</MaterialProperties>")
        {
            break;
        }
    }
    file.close();
    return dataBase;
}

void DataBaseReader::trimSpaces(QString &str)
{
    int j = 0;
    if(str.isEmpty()){
        return;
    }
    for (int i=0; i<str.size();i++){
        if(str[i] == ' ' || str[i] == '\t'){
            j++;
        }
        else
        {
            str.remove(0,j);
            break;
        }
        if(i == str.size()-1){
            str.clear();
            return;
        }
    }
    j = 0;
    for(int i=str.size()-1; i>=0;i--){
        if(str[i] == ' ' ||  str[i] == '\t'){
            j++;
        }
        else
        {
            str.remove(str.size()-1,j);
            break;
        }
    }
}
