#ifndef DATABASEREADER_H
#define DATABASEREADER_H

#include <QObject>
#include <QXmlStreamReader>
#include <QAbstractTableModel>
#include "materialmodel.h"

class DataBaseReader : public QObject
{
    Q_OBJECT
public:
    explicit DataBaseReader(QObject *parent = 0);
    void load(const QString &fileName, MaterialModel *model);
private:
    void readProperty(QXmlStreamReader& xml);
    void readMaterials(QXmlStreamReader& xml);
    QString readXmlPart(const QString &fileName);
    void trimSpaces(QString& str);
private:
    MaterialModel *m_model;
signals:

public slots:
};

#endif // DATABASEREADER_H
