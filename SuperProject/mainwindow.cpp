#include "mainwindow.h"

#include <QTableView>
#include <QDebug>
#include <QSortFilterProxyModel>

#include "databasereader.h"
#include "materialmodel.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    DataBaseReader *data = new DataBaseReader;
    MaterialModel *model = new MaterialModel;
    data->load("D:\\workspace\\fromSergey\\DataBaseTable/SQW.sls", model);



    qDebug() << model->data(model->index(0, 1), "Energy gap");
    qDebug() << model->data(model->index(1, 1), "Energy gap");
    qDebug() << model->data(model->index(2, 1), "Energy gap");

    QSortFilterProxyModel *proxy = new QSortFilterProxyModel();
    proxy->setSourceModel(model);
    proxy->setFilterKeyColumn(0);
    proxy->setFilterRegExp("InN");

    QTableView *view2 = new QTableView;

    view2->setModel(proxy);
    view2->show();


    QTableView *view = new QTableView;

    view->setModel(model);
    setCentralWidget(view);

}

MainWindow::~MainWindow()
{

}
