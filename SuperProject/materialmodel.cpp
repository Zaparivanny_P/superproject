#include "materialmodel.h"
#include <QDebug>

MaterialModel::MaterialModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QVariant MaterialModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant value;
    if(Qt::DisplayRole == role)
    {
        if(Qt::Horizontal == orientation)
        {
            value = m_roleNames.value(section + Qt::UserRole);
        }
        else
        {

        }

    }
    return value;
}

bool MaterialModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{

    if(Qt::Horizontal == orientation)
    {
        m_roleNames.insert(section + Qt::UserRole, value.toByteArray());
        emit headerDataChanged(orientation, section, section);
    }
    else
    {
    }

    return true;

}

int MaterialModel::rowCount(const QModelIndex &parent) const
{

    return m_list.size();
}

int MaterialModel::columnCount(const QModelIndex &parent) const
{


    return m_column;
}

QVariant MaterialModel::data(const QModelIndex &index, int role) const
{
    QVariant result;
    if (!index.isValid())
        return result;

    if(role == Qt::DisplayRole)
    {
        result = m_list.at(index.row())->at(index.column());
    }
    else if(role >= Qt::UserRole)
    {

        result = m_list.at(index.row())->at(role - Qt::UserRole);
    }

    return result;
}

QVariant MaterialModel::data(const QModelIndex &index, QByteArray role) const
{
    QVariant result;
    int r = m_roleNames.key(role, -1);

    if(r != -1)
    {
        result = data(index, r);
    }
    return result;

}

bool MaterialModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {

        if(role == Qt::EditRole)
        {

            (*m_list.at(index.row()))[index.column()].setValue(value);

            emit dataChanged(index, index, QVector<int>() << role);
            return true;
        }
        else if(role >= Qt::EditRole)
        {

            (*m_list.at(index.row()))[role - Qt::UserRole].setValue(value);

            emit dataChanged(this->index(index.row(), role - Qt::UserRole),
                             this->index(index.row(), role - Qt::UserRole));
        }
    }
    return false;
}

bool MaterialModel::setData(const QModelIndex &index, const QVariant &value, QByteArray role)
{
    int r = m_roleNames.key(role, -1);

    if(r != -1)
    {
        return setData(index, value, r);
    }
    else
    {
        return false;
    }
}


bool MaterialModel::insertRows(int row, int count, const QModelIndex &parent)
{
    /*if(m_list.size() < row + count - 1)
    {
        return false;
    }*/
    beginInsertRows(parent, row, row + count - 1);
    for(int i = 0; i < count; i++)
    {
        QVector<QVariant> *vector = new QVector<QVariant>();
        vector->resize(m_column);
        m_list.insert(row + i, vector);
    }
    endInsertRows();
    return true;
}

bool MaterialModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    m_column += count;
    for(auto it : m_list)
    {
        for(int i = 0; i < count; i++)
        {
            it->insert(column + i, QVariant());
        }
    }

    endInsertColumns();
    return true;
}

QHash<int, QByteArray> MaterialModel::roleNames() const
{
    return m_roleNames;
}
