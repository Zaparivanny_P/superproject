#ifndef MATERIALMODEL_H
#define MATERIALMODEL_H

#include <QAbstractTableModel>

class MaterialModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit MaterialModel(QObject *parent = 0);

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = 0);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QVariant data(const QModelIndex &index, QByteArray role) const;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    bool setData(const QModelIndex &index, const QVariant &value,
                 QByteArray role);

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;


    QHash<int, QByteArray> roleNames() const;

private:
    QList<QVector<QVariant>*> m_list;
    int m_column = 0;
    QHash<int, QByteArray> m_roleNames;

};

#endif // MATERIALMODEL_H
